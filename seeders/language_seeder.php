<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
use App\Model\Language;
use Hyperf\Database\Seeders\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Language::create([
            'name' => 'Java',
            'slug' => 'java',
            'icon_url' => 'https://cdn-icons-png.flaticon.com/512/226/226777.png',
        ]);

        Language::create([
            'name' => 'PHP',
            'slug' => 'php',
            'icon_url' => 'https://cdn-icons-png.flaticon.com/512/919/919830.png',
        ]);

        Language::create([
            'name' => 'C#',
            'slug' => 'c-sharp',
            'icon_url' => 'https://cdn-icons-png.flaticon.com/512/919/919830.png',
        ]);
    }
}
