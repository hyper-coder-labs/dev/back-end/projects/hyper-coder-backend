<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */

use App\Model\Exercise;
use App\Model\Language;
use Hyperf\Database\Seeders\Seeder;

class ExerciseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $language = Language::create([
            'name' => 'Go',
            'slug' => 'go',
            'icon_url' => 'https://cdn-icons-png.flaticon.com/512/919/919830.png',
        ]);

        Exercise::create([
            'author_id' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'language_id' => $language->id,
            'slug' => 'hello-world',
            'title' => 'Olá mundo',
            'subtitle' => 'Um programa simples para exibir "Olá mundo!"',
            'image' => 'https://fastly.picsum.photos/id/238/200/300.jpg',
            'description' => 'Esse é um exercício simples de programação que vai lhe ensinar os conceitos básicos de como escrever um programa em PHP.',
        ]);

        Exercise::create([
            'author_id' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'language_id' => $language->id,
            'slug' => 'calculator',
            'title' => 'Calculadora',
            'subtitle' => 'Um programa simples para calcular a soma, subtração, multiplicação e divisão de dois números',
            'image' => 'https://fastly.picsum.photos/id/239/200/300.jpg',
            'description' => 'Esse é um exercício mais avançado de programação que vai lhe ensinar como usar variáveis, operadores e funções para criar um programa que pode realizar cálculos matemáticos.',
        ]);

        Exercise::create([
            'author_id' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'language_id' => $language->id,
            'slug' => 'guess-the-number',
            'title' => 'Advinhe o número',
            'subtitle' => 'Um jogo simples onde o usuário tenta adivinhar um número aleatório',
            'image' => 'https://fastly.picsum.photos/id/240/200/300.jpg',
            'description' => 'Esse é um exercício divertido de programação que vai lhe ensinar como usar loops, condições e feedback para criar um jogo interativo.',
        ]);

        Exercise::create([
            'author_id' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'language_id' => $language->id,
            'slug' => 'to-do-list',
            'title' => 'Lista de tarefas',
            'subtitle' => 'Um programa simples para criar e gerenciar uma lista de tarefas',
            'image' => 'https://fastly.picsum.photos/id/241/200/300.jpg',
            'description' => 'Esse é um exercício útil de programação que vai lhe ensinar como usar arquivos, bancos de dados e formulários para criar um programa que pode ser usado para gerenciar tarefas.',
        ]);

        Exercise::create([
            'author_id' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'language_id' => $language->id,
            'slug' => 'rock-paper-scissors',
            'title' => 'Pedra, papel e tesoura',
            'subtitle' => 'Um jogo clássico onde dois jogadores escolhem entre pedra, papel ou tesoura para lutar',
            'image' => 'https://fastly.picsum.photos/id/242/200/300.jpg',
            'description' => 'Esse é um exercício desafiador de programação que vai lhe ensinar como usar classes, objetos e orientação a objetos para criar um jogo complexo.',
        ]);
    }
}