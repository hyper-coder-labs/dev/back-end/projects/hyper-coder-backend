<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
return [
    'locale' => 'pt_BR',
    'fallback_locale' => 'pt_BR',
    'path' => BASE_PATH . '/storage/languages',
];
