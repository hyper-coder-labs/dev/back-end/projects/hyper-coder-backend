<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
use App\Middleware\KeycloakAuthMiddleware;
use GuzzleHttp\Client;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Logger\LoggerFactory;
use HyperfTest\Mocks\KeycloakAuthMiddlewareMock;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use function Hyperf\Config\config;

return [
    LoggerInterface::class => function (ContainerInterface $container) {
        $appName = config('app_name');
        $factory = $container->get(LoggerFactory::class);

        return $factory->get($appName);
    },
    KeycloakAuthMiddleware::class => function (ContainerInterface $container) {
        return new KeycloakAuthMiddlewareMock(
            client: $container->get(Client::class),
            config: $container->get(ConfigInterface::class),
            logger: $container->get(LoggerInterface::class)
        );
    },
];
