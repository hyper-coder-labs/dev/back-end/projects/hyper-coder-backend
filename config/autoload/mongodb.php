<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
use function Hyperf\Support\env;

return [
    'default' => [
        'mode' => 0,
        'settings' => [
            0 => [
                'host' => env('MONGODB_HOST', '127.0.0.1'),
                'port' => env('MONGODB_PORT', 27017),
                'username' => env('MONGODB_USERNAME', ''),
                'password' => env('MONGODB_PASSWORD', ''),
                'db' => env('MONGODB_DB', 'hyperf'),
            ],
        ],
        'authMechanism' => 'SCRAM-SHA-256',
        'pool' => [
            'min_connections' => 3,
            'max_connections' => 1000,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('MONGODB_MAX_IDLE_TIME', 60),
        ],
    ],
];
