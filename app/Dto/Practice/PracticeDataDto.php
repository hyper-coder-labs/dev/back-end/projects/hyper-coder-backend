<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class PracticeDataDto implements Arrayable
{
    private PracticeInfoDto $info;

    private PracticeConfigDto $config;

    private PracticeFilesDto $files;

    private PracticeDocsDto $docs;

    public function __construct(PracticeInfoDto $info, PracticeConfigDto $config, PracticeFilesDto $files, PracticeDocsDto $docs)
    {
        $this->info = $info;
        $this->config = $config;
        $this->files = $files;
        $this->docs = $docs;
    }

    public function getInfo(): PracticeInfoDto
    {
        return $this->info;
    }

    public function getConfig(): PracticeConfigDto
    {
        return $this->config;
    }

    public function getFiles(): PracticeFilesDto
    {
        return $this->files;
    }

    public function getDocs(): PracticeDocsDto
    {
        return $this->docs;
    }

    public function toArray(): array
    {
        return [
            'info' => $this->getInfo()->toArray(),
            'config' => $this->getConfig()->toArray(),
            'files' => $this->getFiles()->toArray(),
            'docs' => $this->getDocs()->toArray(),
        ];
    }
}
