<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class PracticeFilesDto implements Arrayable
{
    private array $solution_files;

    private array $test_files;

    private array $example_files;

    public function __construct(array $solution_files, array $test_files, array $example_files)
    {
        $this->solution_files = $solution_files;
        $this->test_files = $test_files;
        $this->example_files = $example_files;
    }

    public function getSolutionFiles(): array
    {
        return $this->solution_files;
    }

    public function getTestFiles(): array
    {
        return $this->test_files;
    }

    public function getExampleFiles(): array
    {
        return $this->example_files;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
