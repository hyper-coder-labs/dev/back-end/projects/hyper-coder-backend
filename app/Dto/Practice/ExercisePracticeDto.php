<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class ExercisePracticeDto implements Arrayable
{
    private string $slug;

    private string $name;

    private int $difficulty;

    private array $topics;

    private PracticeDataDto $data;

    public function __construct(string $slug, string $name, int $difficulty, array $topics, PracticeDataDto $data)
    {
        $this->slug = $slug;
        $this->name = $name;
        $this->difficulty = $difficulty;
        $this->topics = $topics;
        $this->data = $data;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function getData(): PracticeDataDto
    {
        return $this->data;
    }

    public function toArray(): array
    {
        return [
            'slug' => $this->getSlug(),
            'name' => $this->getName(),
            'topics' => $this->getTopics(),
            'difficulty' => $this->getDifficulty(),
            'data' => $this->getData()->toArray(),
        ];
    }
}
