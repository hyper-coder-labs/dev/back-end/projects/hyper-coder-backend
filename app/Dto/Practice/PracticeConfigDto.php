<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class PracticeConfigDto implements Arrayable
{
    private string $blurb;

    /**
     * @var string[]
     */
    private array $authors;

    /**
     * @var string[]
     */
    private array $contributors;

    private string $source;

    private string $source_url;

    private string $default_language;

    private array $translations;

    private array $files;

    /**
     * @param string[] $authors
     * @param string[] $contributors
     */
    public function __construct(string $blurb, array $authors, array $contributors, string $source, string $source_url, string $default_language, array $translations, array $files)
    {
        $this->blurb = $blurb;
        $this->authors = $authors;
        $this->contributors = $contributors;
        $this->source = $source;
        $this->source_url = $source_url;
        $this->default_language = $default_language;
        $this->translations = $translations;
        $this->files = $files;
    }

    public function getBlurb(): string
    {
        return $this->blurb;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }

    public function getContributors(): array
    {
        return $this->contributors;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getSourceUrl(): string
    {
        return $this->source_url;
    }

    public function getDefaultLanguage(): string
    {
        return $this->default_language;
    }

    public function getTranslations(): array
    {
        return $this->translations;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
