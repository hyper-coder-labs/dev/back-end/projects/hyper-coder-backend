<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class PracticeDocsDto implements Arrayable
{
    private array $content;

    public function __construct(array $content)
    {
        $this->content = $content;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function toArray(): array
    {
        return $this->content;
    }
}
