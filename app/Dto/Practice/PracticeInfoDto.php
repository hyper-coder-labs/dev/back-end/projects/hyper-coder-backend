<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Dto\Practice;

use Hyperf\Contract\Arrayable;

class PracticeInfoDto implements Arrayable
{
    private string $path;

    private string $slug;

    private string $name;

    private string $hash;

    public function __construct(string $path, string $slug, string $name, string $hash)
    {
        $this->path = $path;
        $this->slug = $slug;
        $this->name = $name;
        $this->hash = $hash;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
