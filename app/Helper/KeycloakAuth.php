<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Helper;

use Hyperf\Context\Context;
use Hyperf\HttpMessage\Exception\HttpException;
use Psr\Http\Message\ServerRequestInterface;
use Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner;

/**
 * Classe responsável por adicionar alguns métodos relacionados a autenticação do Keycloak.
 */
trait KeycloakAuth
{
    /**
     * Método responsável por capturar as informações do usuário autenticado pelo Keycloak.
     */
    public function getAuth(): KeycloakResourceOwner
    {
        $request = Context::get(ServerRequestInterface::class);
        $resourceOwner = $request->getAttribute('resource_owner');

        if ($resourceOwner instanceof KeycloakResourceOwner) {
            return $resourceOwner;
        }

        throw new HttpException(403, 'Failed to capture authentication information');
    }
}
