<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Request\Solution;

use Hyperf\Swagger\Annotation\Items;
use Hyperf\Swagger\Annotation\Property;
use Hyperf\Swagger\Annotation\Schema;
use Hyperf\Validation\Request\FormRequest;

#[Schema(
    schema: 'SolutionSubmission',
    required: [
        'exercise_id',
        'files',
    ],
    properties: [
        new Property(
            property: 'exercise_id',
            description: 'Esse campo representa o id do exercício',
            type: 'integer',
            example: 1,
        ),
        new Property(
            property: 'files',
            description: 'Esse campo representa os arquivos da solução',
            type: 'array',
            items: new Items(
                required: [
                    'content',
                    'filename',
                ],
                properties: [
                    new Property(
                        property: 'content',
                        description: 'Esse campo representa o conteúdo do arquivo',
                        type: 'string',
                        example: 'print("This line will be printed.")',
                    ),
                    new Property(
                        property: 'filename',
                        description: 'Esse campo representa o mome do arquivo da solução',
                        type: 'string',
                        example: 'hello.py'
                    ),
                ]
            )
        ),
    ]
)]
class SolutionSubmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'exercise_id' => [
                'required',
                'integer',
            ],
            'files' => [
                'required',
                'array',
                'min:1',
            ],
            'files.*.content' => [
                'required',
                'string',
                'min:1',
            ],
            'files.*.filename' => [
                'required',
                'string',
            ],
        ];
    }
}
