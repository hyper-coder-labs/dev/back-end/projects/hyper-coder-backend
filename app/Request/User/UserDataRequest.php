<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class UserDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'bio' => ['required', 'string', 'min:2', 'max:100'],
            'language' => ['required', 'string', 'min:2', 'max:5'],
            'phone_number' => ['required', 'string', 'min:11', 'max:14'],
            'date_birth' => ['required', 'string', 'date'],
            'gender' => ['required', 'string', 'min:2', 'max:15'],
            'address.cep' => ['required', 'string', 'min:8', 'max:9'],
            'links.*' => ['array', 'nullable'],
            'links.*.text' => ['string', 'required', 'min:2', 'max:20'],
            'links.*.link' => ['string', 'required', 'min:2'],
        ];
    }

    public function messages(): array
    {
        return [
            'date_birth.required' => 'O campo data de nascimento é obrigatório.',
        ];
    }
}
