<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Request\Exercise;

use App\Constant\LanguagesConstant;
use App\Service\Exercise\ParseExerciseProcessingDto;
use Hyperf\Validation\Request\FormRequest;

class ExerciseProcessingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hash' => ['required', 'string'],
            'version' => ['required', 'string'],
            'project' => ['required', 'array'],
            'project.language' => ['required', 'string'],

            'practices' => ['required', 'array'],
            'practices.*.slug' => ['required', 'string'],
            'practices.*.name' => ['required', 'string'],
            'practices.*.difficulty' => ['required', 'integer', 'min:0', 'max:10'],
            'practices.*.topics' => ['required', 'array'],
            'practices.*.data' => ['required', 'array'],

            'practices.*.data.info' => ['required', 'array'],
            'practices.*.data.info.path' => ['required', 'string'],
            'practices.*.data.info.slug' => ['required', 'string'],
            'practices.*.data.info.name' => ['required', 'string'],
            'practices.*.data.info.hash' => ['required', 'string'],

            'practices.*.data.config' => ['required', 'array'],
            'practices.*.data.config.blurb' => ['required', 'min:5', 'max:100'],
            'practices.*.data.config.authors' => ['required', 'array'],
            'practices.*.data.config.contributors' => ['nullable', 'array'],
            'practices.*.data.config.files' => ['required', 'array'],
            'practices.*.data.config.files.solution' => ['required', 'array'],
            'practices.*.data.config.files.test' => ['required', 'array'],
            'practices.*.data.config.files.example' => ['required', 'array'],
            'practices.*.data.config.source' => ['required', 'min:5'],
            'practices.*.data.config.source_url' => ['required', 'url:https'],
            'practices.*.data.config.default_language' => ['required', 'in:' . LanguagesConstant::getLanguagesInLine()],
            'practices.*.data.config.translations' => ['nullable', 'array'],
            'practices.*.data.config.translations.*' => ['nullable', 'in:' . LanguagesConstant::getLanguagesInLine()],

            'practices.*.data.files' => ['required', 'array'],
            'practices.*.data.files.solution_files' => ['required', 'array'],
            'practices.*.data.files.solution_files.*.name' => ['required', 'string'],
            'practices.*.data.files.solution_files.*.raw' => ['required', 'string'],

            'practices.*.data.files.test_files' => ['required', 'array'],
            'practices.*.data.files.test_files.*.name' => ['required', 'string'],
            'practices.*.data.files.test_files.*.raw' => ['required', 'string'],

            'practices.*.data.files.example_files' => ['required', 'array'],
            'practices.*.data.files.example_files.*.name' => ['required', 'string'],
            'practices.*.data.files.example_files.*.raw' => ['required', 'string'],

            'practices.*.data.docs' => ['nullable', 'array'],
        ];
    }

    public function getDto(): array
    {
        return ParseExerciseProcessingDto::parse($this->all());
    }
}
