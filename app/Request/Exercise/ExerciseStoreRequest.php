<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Request\Exercise;

use Hyperf\Validation\Request\FormRequest;

class ExerciseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'language_id' => [
                'required',
                'integer',
            ],
            'slug' => [
                'required',
                'string',
                'min:2',
                'max:90',
                'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/i',
            ],
            'title' => [
                'required',
                'string',
                'min:5',
                'max:90',
            ],
            'subtitle' => [
                'nullable',
                'string',
                'min:5',
                'max:20',
            ],
            'description' => [
                'required',
                'string',
                'min:20',
                'max:90',
            ],
            'tags' => [
                'required',
                'array',
                'max:5',
            ],
        ];
    }
}
