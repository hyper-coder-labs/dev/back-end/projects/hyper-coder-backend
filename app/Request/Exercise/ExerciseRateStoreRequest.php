<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Request\Exercise;

use Hyperf\Swagger\Annotation\Property;
use Hyperf\Swagger\Annotation\Schema;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

#[Schema(
    schema: 'ExerciseRateStore',
    required: [
        'rate',
        'comment',
    ],
    properties: [
        new Property(
            property: 'rate',
            description: 'Avaliação de 1 a 5 a respeito do exercício',
            type: 'integer',
            example: 1,
        ),
        new Property(
            property: 'comment',
            description: 'Comentário da avaliação do exercício',
            type: 'string',
            example: 'Gostei bastante desse exercício',
        ),
    ]
)]
class ExerciseRateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'rate' => [
                'required',
                Rule::in([1, 2, 3, 4, 5]),
            ],
            'comment' => [
                'required',
                'string',
                'min:2',
                'max:200',
            ],
        ];
    }
}
