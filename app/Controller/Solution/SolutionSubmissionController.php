<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Solution;

use App\Amqp\Producer\SolutionSubmissionProducer;
use App\Controller\AbstractController;
use App\Request\Solution\SolutionSubmissionRequest;
use App\Task\MongoTask;
use Hyperf\Amqp\Producer;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Parameter;
use Hyperf\Swagger\Annotation\Post;
use Hyperf\Swagger\Annotation\RequestBody;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Hyperf\Swagger\Annotation\Schema;
use OpenApi\Attributes\MediaType;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer a validação de uma resposta de um exercício prático.
 */
#[HyperfServer('http')]
#[Controller(prefix: 'solutions')]
class SolutionSubmissionController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Sua resposta está sendo processada';

    private const NAMESPACE_SOLUTIONS = 'solutions.submissions';

    #[Inject]
    public MongoTask $mongoTask;

    #[Inject]
    public Producer $producer;

    #[Post(
        path: '/solutions/{practiceOid}/submissions/',
        summary: 'Validação de uma resposta de um exercício prático',
        security: [['bearerAuth' => []]],
        tags: ['Soluções'],
    )]
    #[Parameter(
        name: 'practiceOid',
        in: 'path',
        required: true,
        example: '64b2f21de796629a0e016552'
    )]
    #[RequestBody(
        description: 'Request parameters',
        content: [
            new MediaType(
                mediaType: 'application/json',
                schema: new Schema(
                    ref: '#/components/schemas/SolutionSubmission'
                )
            ),
        ]
    )]
    #[ResponseOA(response: 200, description: self::MESSAGE_SUCCESS)]
    #[RequestMapping(path: '{practiceOid}/submissions', methods: 'post')]
    public function __invoke(string $practiceOid, SolutionSubmissionRequest $request, Response $response): ResponseInterface
    {
        $submissionId = $this->mongoTask->insertOne(namespace: self::NAMESPACE_SOLUTIONS, document: [
            'exercise_id' => $request->input('exercise_id'),
            'practice_oid' => $practiceOid,
            'files' => $request->input('files'),
        ]);

        $result = $this->producer->produce(
            new SolutionSubmissionProducer($submissionId)
        );

        $data = [
            'submission' => [
                'id' => $submissionId,
                'queued' => $result,
            ],
        ];

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            data: $data
        );
    }
}
