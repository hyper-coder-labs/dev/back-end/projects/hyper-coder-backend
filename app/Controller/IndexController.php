<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller;

use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\HttpServer\Request;
use Hyperf\Stringable\Str;
use Hyperf\Swagger\Annotation\Info;
use Hyperf\Swagger\Annotation\SecurityScheme;
use Hyperf\Swagger\Annotation\Server;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer exibição de uma pequena documentação a respeito dos endpoints.
 */
#[Info(
    description: 'Essa é a API base para a utilização do projeto ',
    title: 'HyperCoder Backend',
)]
#[Server(url: 'http://localhost:9501')]
#[SecurityScheme(
    securityScheme: 'bearerAuth',
    type: 'http',
    name: 'Authorization',
    in: 'header',
    bearerFormat: 'JWT',
    scheme: 'Bearer',
)]
class IndexController extends AbstractController
{
    /**
     * Método responsável por exibir uma pequena documentação a respeito dos endpoints da aplicação.
     * @param Request $request objeto da requisição HTTP
     * @param Response $response objeto de resposta HTTP
     */
    public function index(Request $request, Response $response): ResponseInterface
    {
        return $response->json($this->getRestfulDocs($request->fullUrl()))
            ->withStatus(StatusCodes::HTTP_OK);
    }

    /**
     * Método responsável por gerar uma pequena documentação para seguir os padrões do RESTful.
     * @param string $url URL completa da aplicação
     */
    private function getRestfulDocs(string $url): array
    {
        $url = Str::replace('?', '', $url);

        return [
            'exercises_url' => sprintf('%s/exercises', $url),
            'exercises_search_url' => sprintf('%s/exercises/{exercise_id}', $url),
        ];
    }
}
