<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */

namespace App\Controller\Exercise;

use App\Controller\AbstractController;
use App\Model\Exercise;
use Hyperf\Database\Model\Relations\BelongsToMany;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\HttpServer\Request;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer a exibição da paginação dos exercícios cadastrados.
 */
#[Controller(prefix: '/exercises')]
class ExerciseIndexController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Paginação dos exercícios cadastrados';

    #[RequestMapping(path: '', methods: 'get')]
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        $paginateBuild = Exercise::with(['tags' => fn(BelongsToMany $query) => $query->select('tag')]);

        if ($request->hasInput(['languageId'])) {
            $paginateBuild = $paginateBuild->where('language_id', (int) $request->input('languageId'));
        }

        $paginate = $paginateBuild->paginate(10);

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            attributes: $paginate->toArray(),
        );
    }
}
