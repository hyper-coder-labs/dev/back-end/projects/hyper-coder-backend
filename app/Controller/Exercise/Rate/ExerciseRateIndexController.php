<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise\Rate;

use App\Controller\AbstractController;
use App\Model\ExerciseRate;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\Get;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer uma paginação das avaliações dos exercícios.
 */
#[HyperfServer('http')]
#[Controller('exercises')]
class ExerciseRateIndexController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Paginação da avaliação dos exercícios cadastrados';

    #[Get(
        path: '/exercises/{exercise_id}/rate/',
        summary: 'Paginação das avaliações dos exercícios',
        tags: ['Avaliação']
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_OK,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '{exerciseId}/rate', methods: 'get')]
    public function __invoke(int $exerciseId, Response $response): ResponseInterface
    {
        $paginate = ExerciseRate::where('exercise_id', '=', $exerciseId)
            ->orderByDesc('updated_at')
            ->paginate(10);

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            attributes: $paginate->toArray(),
        );
    }
}
