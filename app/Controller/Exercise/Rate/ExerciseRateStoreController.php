<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise\Rate;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use App\Model\Exercise;
use App\Request\Exercise\ExerciseRateStoreRequest;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Post;
use Hyperf\Swagger\Annotation\RequestBody;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Hyperf\Swagger\Annotation\Schema;
use OpenApi\Attributes\MediaType;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por criar ou atualizar um exercício.
 */
#[HyperfServer('http')]
#[Controller(prefix: 'exercises')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class ExerciseRateStoreController extends AbstractController
{
    use KeycloakAuth;

    public const MESSAGE_SUCCESS = 'Exercício criado com sucesso';

    #[Post(
        path: '/exercises/{exercise_id}/rate/',
        summary: 'Cadastrar ou atualizar uma nova versão em um exercício',
        security: [['bearerAuth' => []]],
        tags: ['Avaliação']
    )]
    #[RequestBody(
        description: 'Request parameters',
        content: [
            new MediaType(
                mediaType: 'application/json',
                schema: new Schema(
                    ref: '#/components/schemas/ExerciseRateStore'
                )
            ),
        ]
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_OK,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '{exerciseId}/rate', methods: 'post')]
    public function __invoke(int $exerciseId, ExerciseRateStoreRequest $request, Response $response): ResponseInterface
    {
        /**
         * @var null|Exercise $exercise
         */
        $exercise = Exercise::find($exerciseId);
        if ($exercise === null) {
            return $this->abort404($response);
        }

        $rateData = $request->inputs(['rate', 'comment']);
        $exerciseRate = $exercise->rates()->updateOrCreate(
            attributes: [
                'user_id' => $this->getAuth()->getId(),
                'exercise_id' => $exercise->id,
            ],
            values: $rateData
        );

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_CREATED,
            message: self::MESSAGE_SUCCESS,
            data: $exerciseRate->toArray()
        );
    }
}
