<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise\Practice;

use App\Controller\AbstractController;
use App\Task\MongoTask;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\Get;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer exibição de todos os exercícios práticos.
 */
#[HyperfServer('http')]
#[Controller(prefix: 'exercises')]
class PracticeIndexController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Paginação dos exercícios práticos';

    #[Inject]
    public MongoTask $mongoTask;

    #[Get(
        path: '/exercises/{exercise_id}/practices/',
        summary: 'Exibição de todos os exercícios práticos',
        tags: ['Práticas']
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_OK,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '{exerciseId}/practices', methods: 'get')]
    public function __invoke(int $exerciseId, Response $response): ResponseInterface
    {
        $namespace = sprintf('exercise.practices.%s', $exerciseId);
        $practices = $this->mongoTask->find(namespace: $namespace, filter: [], options: [
            'projection' => [
                'name' => 1,
                'difficulty' => 1,
                'slug' => 1,
                'topics' => 1,
                'exercise_id' => 1,
            ],
        ]);

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            data: $practices->toArray(),
        );
    }
}
