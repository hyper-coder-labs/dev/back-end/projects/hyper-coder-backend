<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise;

use App\Controller\AbstractController;
use App\Model\Exercise;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\Get;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Parameter;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer a exibição de um exercício individualmente.
 */
#[HyperfServer('http')]
#[Controller('exercises')]
class ExerciseShowController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Listagem das informações do exercício';

    #[Get(
        path: '/exercises/{exerciseId}/',
        summary: 'Exibição de um único exercício',
        tags: ['Exercícios']
    )]
    #[Parameter(
        name: 'exerciseId',
        in: 'path',
        required: true,
        example: 1
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_OK,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '{exerciseId}', methods: 'get')]
    public function __invoke(int $exerciseId, Response $response): ResponseInterface
    {
        /**
         * @var null|Exercise $exercise
         */
        $exercise = Exercise::with('tags')->find($exerciseId);
        if ($exercise === null) {
            return $this->abort404($response);
        }

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            data: $exercise?->toArray()
        );
    }
}
