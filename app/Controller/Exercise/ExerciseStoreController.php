<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use App\Model\Exercise;
use App\Model\Tag;
use App\Request\Exercise\ExerciseStoreRequest;
use Hyperf\Collection\Arr;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Stringable\Str;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Post;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por fazer a criação de um novo exercício.
 */
#[HyperfServer('http')]
#[Controller(prefix: 'exercises')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class ExerciseStoreController extends AbstractController
{
    use KeycloakAuth;

    public const MESSAGE_SUCCESS = 'Exercício criado com sucesso';

    #[Post(
        path: '/exercises/',
        summary: 'Criação de um novo exercício',
        security: [['bearerAuth' => []]],
        tags: ['Exercícios']
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_CREATED,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '', methods: 'post')]
    public function __invoke(ExerciseStoreRequest $request, Response $response): ResponseInterface
    {
        $exercise = Exercise::create([
            'author_id' => $this->getAuth()->getId(),
            'language_id' => $request->input('language_id'),
            'slug' => $request->input('slug'),
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'description' => $request->input('description'),
        ]);

        $this->createExerciseTags($exercise, $request->input('tags'));
        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_CREATED,
            message: self::MESSAGE_SUCCESS,
            data: $exercise->toArray()
        );
    }

    /**
     * Método responsável por preparar as tags e fazer a criação associando a um exercício.
     */
    private function createExerciseTags(Exercise $exercise, array $tags): void
    {
        for ($i = 0; $i < sizeof($tags); ++$i) {
            $tags[$i] = Str::lower($tags[$i]);
            $tags[$i] = Str::slug($tags[$i]);
        }

        $tags = Arr::unique($tags);
        foreach ($tags as $tag) {
            $tagId = Tag::firstOrCreate(['tag' => $tag])->id;
            $exercise->tags()->attach($tagId);
        }
    }
}
