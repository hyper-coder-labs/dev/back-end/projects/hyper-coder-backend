<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use App\Model\Exercise;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Swagger\Annotation\Delete;
use Hyperf\Swagger\Annotation\HyperfServer;
use Hyperf\Swagger\Annotation\Parameter;
use Hyperf\Swagger\Annotation\Response as ResponseOA;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por excluir um exercício.
 */
#[HyperfServer('http')]
#[Controller(prefix: 'exercises')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class ExerciseDestroyController extends AbstractController
{
    use KeycloakAuth;

    public const MESSAGE_SUCCESS = 'Exercício excluído com sucesso';

    #[Delete(
        path: '/exercises/{exerciseId}/',
        summary: 'Exclusão de um exercício',
        security: [['bearerAuth' => []]],
        tags: ['Exercícios']
    )]
    #[Parameter(
        name: 'exerciseId',
        in: 'path',
        required: true,
        example: 1
    )]
    #[ResponseOA(
        response: StatusCodes::HTTP_OK,
        description: self::MESSAGE_SUCCESS
    )]
    #[RequestMapping(path: '{exerciseId}', methods: 'delete')]
    public function __invoke(int $exerciseId, Response $response): ResponseInterface
    {
        /**
         * @var null|Exercise $exercise
         */
        $exercise = Exercise::find($exerciseId);
        if ($exercise === null || $exercise?->author_id != $this->getAuth()->getId()) {
            return $this->abort404($response);
        }

        $exercise->delete();

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
        );
    }
}
