<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Exercise;

use App\Controller\AbstractController;
use App\Dto\Practice\ExercisePracticeDto;
use App\Model\Exercise;
use App\Request\Exercise\ExerciseProcessingRequest;
use App\Task\MongoTask;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Controle responsável por processar informações dos exercícios.
 */
#[Controller(prefix: 'exercises')]
class ExerciseProcessingController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Exercício processado com sucesso';

    #[Inject]
    public MongoTask $mongoTask;

    /**
     * Método responsável por processar as informações enviadas de um exercício.
     * @param int $exerciseId valor do ID referente ao exercício que será buscado no banco de dados
     * @param ExerciseProcessingRequest $request objeto contendo as informações da requisição HTTP
     * @param Response $response objeto de resposta HTTP
     */
    #[RequestMapping(path: '{exerciseId}/processing', methods: 'post')]
    public function __invoke(int $exerciseId, ExerciseProcessingRequest $request, Response $response): ResponseInterface
    {
        Exercise::findOrFail($exerciseId);

        $namespace = sprintf('exercise.practices.%s', $exerciseId);
        $this->mongoTask->deleteMany($namespace, []);

        /**
         * @var ExercisePracticeDto $practice
         */
        foreach ($request->getDto() as $practice) {
            $content = $practice->toArray();
            $content['exercise_id'] = $exerciseId;
            $content['hash'] = md5(json_encode($content));

            $this->mongoTask->insertOne($namespace, $content);
        }

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
        );
    }
}
