<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\User;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use App\Model\Address\Country;
use App\Model\Address\Municipality;
use App\Model\Address\State;
use App\Model\User\UserAdress;
use App\Model\User\UserData;
use App\Model\User\UserLink;
use App\Request\User\UserDataRequest;
use App\Service\Address\AddressService;
use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\Stringable\Str;
use Hyperf\Swagger\Annotation\HyperfServer;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

#[HyperfServer('http')]
#[Controller(prefix: 'user')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class UserUpdateController extends AbstractController
{
    use KeycloakAuth;

    #[Inject]
    protected AddressService $addressService;

    #[RequestMapping(path: 'info/', methods: 'put')]
    public function __invoke(UserDataRequest $request, Response $response): ResponseInterface
    {
        $userDataRequest = [
            'bio' => $request->input('bio'),
            'language' => $request->input('language'),
            'phone_number' => $request->input('phone_number'),
            'gender' => $request->input('gender', 'male'),
            'date_birth' => Carbon::createFromFormat('Y-m-d', $request->input('date_birth')),
        ];

        UserData::updateOrCreate(['user_id' => $this->getAuth()->getId()], $userDataRequest);

        $userSocial = $request->input('links', []);
        $userAddress = $request->input('address.cep');
        $userId = $this->getAuth()->getId();

        go(fn () => $this->saveUserAddress($userAddress, $userId));
        go(fn () => $this->saveUserLink($userSocial, $userId));

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: 'Informações do usuário atualizadas',
        );
    }

    private function saveUserLink(array $data, string $userId): bool
    {
        foreach ($data as $item) {
            UserLink::updateOrCreate([
                'user_id' => $userId,
                'text' => Str::lower($item['text']),
            ], [
                'link' => $item['link'],
            ]);
        }

        return true;
    }

    private function saveUserAddress(string $cep, string $userId): void
    {
        $cepData = $this->addressService->findCepData($cep);

        /** @var Country $country */
        $country = Country::updateOrCreate(['code' => 'BR'], [
            'name' => 'Brasil',
        ]);

        $stateInfo = ['country_id' => $country->id];

        if (State::where('code', $cepData['state'])->limit(1)->count() <= 0) {
            $stateData = $this->addressService->findStateData($cepData['state']);
            $stateInfo['name'] = $stateData['nome'];
        }

        /** @var State $state */
        $state = State::updateOrCreate(['code' => $cepData['state']], $stateInfo);

        /** @var Municipality $municipality */
        $municipality = Municipality::updateOrCreate(['name' => $cepData['city']], [
            'state_id' => $state->id,
        ]);

        UserAdress::updateOrCreate(['user_id' => $userId], [
            'municipality_id' => $municipality->id,
            'cep' => $cepData['cep'],
        ]);
    }
}
