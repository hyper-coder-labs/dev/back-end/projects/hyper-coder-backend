<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\User;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use App\Model\User\UserData;
use Hyperf\Database\Model\Relations\HasMany;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\HttpServer\Request;
use Hyperf\Swagger\Annotation\HyperfServer;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

#[HyperfServer('http')]
#[Controller(prefix: 'user')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class UserShowController extends AbstractController
{
    use KeycloakAuth;

    private const DEFAULT_LANGUAGE = 'pt_BR';

    #[RequestMapping(path: 'info/', methods: 'get')]
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        $userData = $this->findUserData();

        if ($userData) {
            return $this->toJson(
                response: $response,
                statusCode: StatusCodes::HTTP_OK,
                message: 'Informações recuperadas',
                data: $userData->toArray()
            );
        }

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: 'Informações default do usuário',
            data: $this->getDefaultUserData()
        );
    }

    private function findUserData(): ?UserData
    {
        $userData = UserData::where('user_id', $this->getAuth()->getId())
            ->with(['links' => function (HasMany $query) {
                $query->select('user_id', 'text', 'link');
            }])
            ->with('address')
            ->first();

        if ($userData instanceof UserData) {
            return $userData;
        }

        return null;
    }

    private function getDefaultUserData(): array
    {
        return [
            'bio' => null,
            'language' => self::DEFAULT_LANGUAGE,
            'phone_number' => null,
            'gender' => 'male',
            'date_birth' => null,
            'links' => null,
            'address' => null,
        ];
    }
}
