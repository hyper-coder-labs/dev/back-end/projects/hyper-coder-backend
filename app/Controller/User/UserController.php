<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\User;

use App\Controller\AbstractController;
use App\Helper\KeycloakAuth;
use App\Middleware\KeycloakAuthMiddleware;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Hyperf\HttpServer\Request;
use Hyperf\Swagger\Annotation\HyperfServer;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

#[HyperfServer('http')]
#[Controller(prefix: 'user')]
#[Middleware(middleware: KeycloakAuthMiddleware::class)]
class UserController extends AbstractController
{
    use KeycloakAuth;

    #[RequestMapping(path: 'me/', methods: 'get')]
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: 'Informações do usuário',
            data: $this->getAuth()->toArray()
        );
    }
}
