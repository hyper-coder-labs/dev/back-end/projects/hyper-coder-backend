<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller\Language;

use App\Controller\AbstractController;
use App\Model\Language;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

#[Controller(prefix: '/languages')]
class LanguageIndexController extends AbstractController
{
    public const MESSAGE_SUCCESS = 'Paginação das linguagens cadastrados';

    #[RequestMapping(path: '', methods: 'get')]
    public function __invoke(Response $response): ResponseInterface
    {
        $paginate = Language::orderByDesc('name')
            ->paginate(10);

        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_OK,
            message: self::MESSAGE_SUCCESS,
            attributes: $paginate->toArray(),
        );
    }
}
