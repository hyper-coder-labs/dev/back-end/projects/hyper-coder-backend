<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Controller;

use App\Entity\ResponseEntity;
use Hyperf\HttpServer\Contract\ResponseInterface as Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Classe responsável por fazer a abstração de alguns métodos essenciais para o Controller.
 */
abstract class AbstractController
{
    public const MESSAGE_NOT_FOUND = 'A informação procurada não está mais disponível';

    /**
     * Retorna as informações utilizando o padrão do ResponseEntity.
     *
     * @param Response $response objeto de resposta HTTP
     * @param int $statusCode código do status HTTP para a resposta
     * @param string $message mensagem que será retornada ao usuário
     * @param array $data conjunto de informações que serão retornadas ao usuário
     * @param array $attributes atributos adicionais que podem ser incorporados à resposta da requisição
     */
    protected function toJson(
        Response $response,
        int $statusCode = 200,
        string $message = 'OK',
        array $data = [],
        array $attributes = []
    ): ResponseInterface {
        $responseEntity = new ResponseEntity(
            $statusCode,
            $message,
            $data,
            $attributes
        );

        return $response->json($responseEntity->toArray())
            ->withStatus($statusCode);
    }

    /**
     * Retorna uma resposta de 404.
     *
     * @param Response $response objeto de resposta HTTP
     */
    protected function abort404(Response $response): ResponseInterface
    {
        return $this->toJson(
            response: $response,
            statusCode: StatusCodes::HTTP_NOT_FOUND,
            message: self::MESSAGE_NOT_FOUND,
        );
    }
}
