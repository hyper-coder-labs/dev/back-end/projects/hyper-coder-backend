<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Hyperf\Context\Context;
use Hyperf\Contract\ConfigInterface;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * Middleware para validação da autenticação com o Keycloak.
 */
class KeycloakAuthMiddleware implements MiddlewareInterface
{
    /**
     * Construtor responsável por capturados dependências do middleware.
     */
    public function __construct(
        protected Client $client,
        protected ConfigInterface $config,
        protected LoggerInterface $logger
    ) {
    }

    /**
     * Método por processar as informações de autenticação.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $regexToken = '/^Bearer ((?:\.?[A-Za-z0-9-_]+){3})$/m';
        $accessToken = $request->getHeader('authorization')[0] ?? '';

        if (! $request->hasHeader('authorization') || ! preg_match_all($regexToken, $accessToken, $matches, PREG_SET_ORDER)) {
            return $this->handleUnauthorizedException();
        }

        $accessToken = str_replace('Bearer', '', $accessToken);
        $accessToken = trim($accessToken);

        $keycloakRealm = $this->config->get('keycloak_realm');
        $keycloakServerUrl = $this->config->get('keycloak_auth_server_url');

        $uri = sprintf('%s/auth/realms/%s/protocol/openid-connect/userinfo', $keycloakServerUrl, $keycloakRealm);

        try {
            $resp = $this->client->get($uri, [
                'headers' => [
                    'authorization' => "Bearer {$accessToken}",
                ],
            ]);

            if ($resp->getStatusCode() == StatusCodes::HTTP_OK) {
                $data = json_decode($resp->getBody()->getContents(), true);
                $resourceOwner = new KeycloakResourceOwner($data);

                $request = $request->withAttribute('resource_owner', $resourceOwner);
                return $handler->handle($request);
            }
        } catch (ClientException $exception) {
            $this->logger->error(sprintf('Ocorreu um erro na autenticação: %s', $exception->getMessage()));
        }

        return $this->handleUnauthorizedException();
    }

    protected function handleUnauthorizedException(): ResponseInterface
    {
        return Context::override(ResponseInterface::class, function (ResponseInterface $response) {
            $data = json_encode([
                'error' => 'invalid_token',
                'error_description' => 'Token verification failed',
            ]);

            return $response
                ->withStatus(StatusCodes::HTTP_UNAUTHORIZED)
                ->withAddedHeader('content-type', 'application/json; charset=utf-8')
                ->withBody(new SwooleStream($data));
        });
    }
}
