<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $exercise_id
 * @property string $user_id
 * @property int $rate
 * @property string $comment
 * @property int $approved
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ExerciseRate extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'exercise_rates';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [
        'exercise_id',
        'user_id',
        'rate',
        'comment',
    ];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'exercise_id' => 'integer', 'rate' => 'integer', 'approved' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}
