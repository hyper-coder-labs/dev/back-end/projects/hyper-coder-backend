<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model;

use Carbon\Carbon;
use Hyperf\Database\Model\Relations\BelongsToMany;
use Hyperf\Database\Model\Relations\HasMany;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $author_id
 * @property int $language
 * @property string $slug
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Exercise extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'exercises';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [
        'author_id',
        'language_id',
        'slug',
        'title',
        'subtitle',
        'description',
    ];

    protected array $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = [
        'id' => 'integer',
        'language_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get all the tags for the exercise.
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Get all the rates for the exercise.
     */
    public function rates(): HasMany
    {
        return $this->hasMany(ExerciseRate::class);
    }
}
