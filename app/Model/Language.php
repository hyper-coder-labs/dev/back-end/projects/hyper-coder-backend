<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model;

use Carbon\Carbon;
use Hyperf\Database\Model\Relations\BelongsToMany;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $icon_url
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Language extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'languages';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [
        'name',
        'slug',
        'icon_url',
    ];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }
}
