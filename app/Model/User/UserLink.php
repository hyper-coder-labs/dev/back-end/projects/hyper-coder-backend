<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\User;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $user_id
 * @property string $text
 * @property string $link
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class UserLink extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_links';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['user_id', 'text', 'link'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    protected array $hidden = ['user_id'];
}
