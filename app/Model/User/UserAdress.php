<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\User;

use App\Model\Address\Municipality;
use Carbon\Carbon;
use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $user_id
 * @property int $municipality_id
 * @property string $cep
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class UserAdress extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_adresses';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['user_id', 'municipality_id', 'cep'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'municipality_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    protected array $hidden = ['user_id'];

    public function municipality(): BelongsTo
    {
        return $this->belongsTo(Municipality::class)
            ->select('id', 'state_id', 'name')
            ->with('state');
    }
}
