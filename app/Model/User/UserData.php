<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\User;

use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\Database\Model\Relations\HasMany;
use Hyperf\DbConnection\Model\Model;

class UserData extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_datas';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['user_id', 'bio', 'language', 'phone_number', 'date_birth', 'gender'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = [
        'date_birth' => 'date',
    ];

    protected array $hidden = ['id', 'user_id'];

    public function links(): HasMany
    {
        return $this->hasMany(
            related: UserLink::class,
            foreignKey: 'user_id',
            localKey: 'user_id',
        );
    }

    public function address(): BelongsTo
    {
        return $this->belongsTo(
            related: UserAdress::class,
            foreignKey: 'user_id',
            ownerKey: 'user_id'
        )->with('municipality');
    }
}
