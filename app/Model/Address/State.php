<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\Address;

use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\DbConnection\Model\Model;

class State extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'states';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['country_id', 'code', 'name'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = [];

    protected array $hidden = ['id'];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class)
            ->select('id', 'code', 'name');
    }
}
