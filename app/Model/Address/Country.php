<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\Address;

use Hyperf\DbConnection\Model\Model;

class Country extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'countries';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['code', 'name'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = [];

    protected array $hidden = ['id'];
}
