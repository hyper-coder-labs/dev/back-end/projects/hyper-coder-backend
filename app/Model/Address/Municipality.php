<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Model\Address;

use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\DbConnection\Model\Model;

class Municipality extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'municipalities';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['state_id', 'name'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = [];

    protected array $hidden = ['id'];

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class)
            ->select('id', 'country_id', 'code', 'name')
            ->with('country');
    }
}
