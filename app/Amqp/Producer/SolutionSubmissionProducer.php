<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Amqp\Producer;

use Hyperf\Amqp\Annotation\Producer;
use Hyperf\Amqp\Message\ProducerMessage;

#[Producer(exchange: 'hyperf', routingKey: 'hyperf')]
class SolutionSubmissionProducer extends ProducerMessage
{
    public function __construct($id)
    {
        $this->payload = [
            'submission_id' => $id,
        ];
    }
}
