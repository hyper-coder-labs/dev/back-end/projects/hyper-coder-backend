<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Entity;

use function Hyperf\Support\env;

/**
 * Entidade responsável por encapsular um modelo padrão de retorno de informações.
 */
class ResponseEntity
{
    /**
     * @var int status HTTP para a resposta
     */
    public int $statusCode;

    /**
     * @var string mensagem que será retornada ao usuário
     */
    public string $message;

    /**
     * @var array conjunto de informações que serão retornadas ao usuário
     */
    public array $data;

    /**
     * @var array atributos adicionais que podem ser incorporados à resposta da requisição
     */
    public array $attributes;

    public function __construct(int $statusCode, string $message = 'OK', array $data = [], array $attributes = [])
    {
        $this->statusCode = $statusCode;
        $this->message = $message;
        $this->data = $data;
        $this->attributes = $attributes;
    }

    /**
     * Método responsável por converter as informações para array.
     */
    public function toArray(): array
    {
        return [
            'status' => $this->statusCode,
            'message' => $this->message,
            'data' => $this->data,
            ...$this->attributes,
            'version' => env('APP_VERSION', 'develop'),
        ];
    }
}
