<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Task;

use Hyperf\Context\ApplicationContext;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Task\Annotation\Task;
use MongoDB\BSON\ObjectId;
use MongoDB\Client;
use MongoDB\Database;
use MongoDB\DeleteResult;
use MongoDB\Driver\Cursor;
use MongoDB\InsertManyResult;
use MongoDB\UpdateResult;

class MongoTask
{
    public static Database $database;

    #[Task]
    public function insertOne(string $namespace, array $document): ?ObjectId
    {
        return MongoTask::database()->selectCollection($namespace)
            ->insertOne($document)->getInsertedId();
    }

    #[Task]
    public function insertMany(string $namespace, array $documents): InsertManyResult
    {
        return MongoTask::database()->selectCollection($namespace)
            ->insertMany($documents);
    }

    #[Task]
    public function findOne(string $namespace, array $filter, array $options = []): ?array
    {
        return MongoTask::database()->selectCollection($namespace)
            ->findOne($filter, $options);
    }

    #[Task]
    public function find(string $namespace, array $filter, array $options = []): Cursor
    {
        return MongoTask::database()->selectCollection($namespace)
            ->find($filter, $options);
    }

    #[Task]
    public function updateOne(string $namespace, array $filter, array $update): UpdateResult
    {
        return MongoTask::database()->selectCollection($namespace)
            ->updateOne($filter, $update);
    }

    #[Task]
    public function updateMany(string $namespace, array $filter, array $update): UpdateResult
    {
        return MongoTask::database()->selectCollection($namespace)
            ->updateMany($filter, $update);
    }

    #[Task]
    public function deleteOne(string $namespace, array $filter, array $options = []): DeleteResult
    {
        return MongoTask::database()->selectCollection($namespace)
            ->deleteOne($filter, $options);
    }

    #[Task]
    public function deleteMany(string $namespace, array $filter, array $options = []): DeleteResult
    {
        return MongoTask::database()->selectCollection($namespace)
            ->deleteMany($filter, $options);
    }

    public static function init(): void
    {
        /** @var ConfigInterface $config */
        $config = ApplicationContext::getContainer()->get(ConfigInterface::class);
        $default = $config->get('mongodb.default');

        $settings = $default['settings'][$default['mode']];

        $uri = sprintf('mongodb://%s:%s', $settings['host'], $settings['port']);

        $mongodbConfig = [];
        $ignoreConfig = [
            'host',
            'port',
            'db',
        ];

        foreach ($settings as $setting => $value) {
            if (! empty($value) && ! in_array($setting, $ignoreConfig)) {
                $mongodbConfig[$setting] = $value;
            }
        }

        $client = new Client($uri, $mongodbConfig);
        self::$database = $client->selectDatabase($settings['db']);
    }

    public static function database(): Database
    {
        if (empty(self::$database)) {
            self::init();
        }

        return self::$database;
    }
}
