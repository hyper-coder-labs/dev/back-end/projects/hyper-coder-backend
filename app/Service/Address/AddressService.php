<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Service\Address;

use Exception;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

class AddressService
{
    public function __construct(protected Client $client)
    {
    }

    public function findCepData(string $cep): array
    {
        $cep = preg_replace('/[^0-9]/', '', $cep);
        $uri = sprintf('https://brasilapi.com.br/api/cep/v1/%s', $cep);

        $response = $this->client->get($uri);
        if ($response->getStatusCode() == StatusCodes::HTTP_OK) {
            return json_decode($response->getBody()->getContents(), true);
        }

        throw new Exception('Não foi possível consultar as informações do CEP');
    }

    public function findStateData(string $uf): array
    {
        $uri = sprintf('http://servicodados.ibge.gov.br/api/v1/localidades/estados/%s', $uf);
        $response = $this->client->get($uri);

        if ($response->getStatusCode() == StatusCodes::HTTP_OK) {
            return json_decode($response->getBody()->getContents(), true);
        }

        throw new Exception('Não foi possível consultar as informações do estado');
    }
}
