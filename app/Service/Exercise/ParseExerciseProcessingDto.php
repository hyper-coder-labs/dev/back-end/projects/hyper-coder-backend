<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace App\Service\Exercise;

use App\Dto\Practice\ExercisePracticeDto;
use App\Dto\Practice\PracticeConfigDto;
use App\Dto\Practice\PracticeDataDto;
use App\Dto\Practice\PracticeDocsDto;
use App\Dto\Practice\PracticeFilesDto;
use App\Dto\Practice\PracticeInfoDto;

class ParseExerciseProcessingDto
{
    /**
     * @return ExercisePracticeDto[]
     */
    public static function parse(array $data): array
    {
        return (new ParseExerciseProcessingDto())
            ->parseDto($data);
    }

    /**
     * @return ExercisePracticeDto[]
     */
    public function parseDto(array $data): array
    {
        $practices = [];

        foreach ($data['practices'] as $practice) {
            $practices[] = new ExercisePracticeDto(
                slug: $practice['slug'],
                name: $practice['name'],
                difficulty: $practice['difficulty'],
                topics: $practice['topics'],
                data: $this->parsePracticeDataDto($practice['data'])
            );
        }

        return $practices;
    }

    private function parsePracticeDataDto(array $practice): PracticeDataDto
    {
        return new PracticeDataDto(
            info: $this->parsePracticeInfoDto($practice['info']),
            config: $this->parsePracticeConfigDto($practice['config']),
            files: $this->parsePracticeFilesDto($practice['files']),
            docs: $this->parsePracticeDocsDto($practice['docs']),
        );
    }

    private function parsePracticeInfoDto(array $info): PracticeInfoDto
    {
        return new PracticeInfoDto(
            path: $info['path'],
            slug: $info['slug'],
            name: $info['name'],
            hash: $info['hash']
        );
    }

    private function parsePracticeConfigDto(array $config): PracticeConfigDto
    {
        return new PracticeConfigDto(
            blurb: $config['blurb'],
            authors: $config['authors'],
            contributors: $config['contributors'],
            source: $config['source'],
            source_url: $config['source_url'],
            default_language: $config['default_language'],
            translations: $config['translations'],
            files: $config['files'],
        );
    }

    private function parsePracticeFilesDto(array $files): PracticeFilesDto
    {
        return new PracticeFilesDto(
            solution_files: $files['solution_files'],
            test_files: $files['test_files'],
            example_files: $files['example_files'],
        );
    }

    private function parsePracticeDocsDto(array $docs): PracticeDocsDto
    {
        return new PracticeDocsDto(
            content: $docs,
        );
    }
}
