init:
	echo "Start"
up:
	docker compose up
down:
	docker compose down -v
php:
	docker exec -it hyper-coder-backend-app bash
teste:
	vendor/bin/co-phpunit --prepend test/bootstrap.php -c phpunit.xml