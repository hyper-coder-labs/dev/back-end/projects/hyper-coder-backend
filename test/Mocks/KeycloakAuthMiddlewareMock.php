<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Mocks;

use App\Middleware\KeycloakAuthMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner;

class KeycloakAuthMiddlewareMock extends KeycloakAuthMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $resourceOwner = new KeycloakResourceOwner([
            'sub' => '3074d846-94a3-4cef-9181-9bd8daa4b926',
            'username' => 'mockuser',
            'email' => 'mockuser@mock.com',
            'email_verified' => true,
        ]);

        $request = $request->withAttribute('resource_owner', $resourceOwner);
        return $handler->handle($request);
    }
}
