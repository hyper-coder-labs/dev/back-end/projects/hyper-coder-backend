<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise\Rate;

use App\Model\Exercise;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\ExerciseRateFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\HyperfTestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseRateIndexControllerFTest extends HyperfTestCase
{
    public function testRateIndexExercise()
    {
        $factoryTotal = 10;

        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'language_id' => $language->id,
            ])
            ->first();

        $this->factory(ExerciseRateFactory::class)
            ->createMany($factoryTotal, [
                'exercise_id' => $exercise->id,
            ])
            ->get();

        /** @var ResponseInterface $response */
        $response = $this->request(
            method: 'GET',
            path: sprintf('/exercises/%s/rate', $exercise->id),
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());

        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('total', $responseContents);
        $this->assertEquals($factoryTotal, $responseContents['total']);
    }
}
