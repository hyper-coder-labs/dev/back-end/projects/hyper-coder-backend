<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise\Rate;

use App\Model\Exercise;
use App\Model\ExerciseRate;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\Helper\KeycloakMock;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseRateStoreControllerFTest extends HyperfTestCase
{
    use KeycloakMock;

    public function testRateStoreExercise(): void
    {
        $data = [
            'rate' => 4,
            'comment' => 'Excelente, abordando POO e outros tópicos a mais',
        ];

        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'language_id' => $language->id,
            ])
            ->first();

        $this->createKeycloakMock();
        $response = $this->request(
            method: 'POST',
            path: sprintf('/exercises/%s/rate', $exercise->id),
            options: [
                'headers' => [
                    'Authorization' => 'Bearer AbCdEf123456',
                    'Content-Type' => 'application/json',
                ],
                'json' => $data,
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_CREATED, $response->getStatusCode());
        $this->assertDatabaseCount(ExerciseRate::class, 1);
        $this->assertDatabaseHas(ExerciseRate::class, $data);
    }
}
