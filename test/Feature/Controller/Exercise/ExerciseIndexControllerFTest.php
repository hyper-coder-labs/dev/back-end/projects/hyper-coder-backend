<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */

namespace HyperfTest\Feature\Controller\Exercise;

use App\Model\Language;
use Hyperf\Collection\Arr;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\Factory\TagFactory;
use HyperfTest\HyperfTestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseIndexControllerFTest extends HyperfTestCase
{
    public function testIndexExercise()
    {
        $factoryTotal = 10;

        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        $this->factory(ExerciseFactory::class)
            ->createMany($factoryTotal, [
                'language_id' => $language->id,
            ])
            ->with(relation: 'tags', factory: TagFactory::class)
            ->first();

        /** @var ResponseInterface $response */
        $response = $this->request(
            method: 'GET',
            path: '/exercises',
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());

        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('total', $responseContents);
        $this->assertEquals($factoryTotal, $responseContents['total']);
    }

    public function testIndexWithFilterExercise()
    {
        $factoryTotal = 10;

        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createMany(5)
            ->map(function(Language $item) use ($factoryTotal){
                $this->factory(ExerciseFactory::class)
                    ->createMany($factoryTotal, [
                        'language_id' => $item->id,
                    ])
                    ->with(relation: 'tags', factory: TagFactory::class)
                    ->first();
            })
            ->first();

        /** @var ResponseInterface $response */
        $response = $this->request(
            method: 'GET',
            path: '/exercises',
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'query' => [
                    'languageId' => $language->id,
                ]
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());

        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('total', $responseContents);
        $this->assertEquals($factoryTotal, $responseContents['total']);

        $this->assertEquals($language->id, $responseContents['data'][0]['language_id']);
    }
}
