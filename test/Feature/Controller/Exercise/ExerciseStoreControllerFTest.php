<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Feature\Controller\Exercise;

use App\Model\Exercise;
use App\Model\Language;
use App\Model\Tag;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\Helper\KeycloakMock;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseStoreControllerFTest extends HyperfTestCase
{
    use KeycloakMock;

    public function testCreateExercise()
    {
        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        $data = [
            'language_id' => $language->id,
            'slug' => 'introducoo-ao-java',
            'title' => 'Introdução ao JAVA',
            'subtitle' => 'Conceitos básicos',
            'description' => 'Java está entre as linguagens de programação disponíveis mais populares.',
            'tags' => [
                'java',
                'basic',
            ],
        ];

        $this->createKeycloakMock();
        $response = $this->request(method: 'POST', path: '/exercises', options: [
            'headers' => [
                'Authorization' => 'Bearer AbCdEf123456',
                'Content-Type' => 'application/json',
            ],
            'json' => $data,
        ]);

        $this->assertEquals(StatusCodes::HTTP_CREATED, $response->getStatusCode());
        $this->assertDatabaseCount(Exercise::class, 1);
        $this->assertDatabaseCount(Tag::class, 2);
    }
}
