<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise;

use App\Model\Exercise;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\Factory\TagFactory;
use HyperfTest\Helper\KeycloakMock;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseDestroyControllerFTest extends HyperfTestCase
{
    use KeycloakMock;

    public function testDestroyExercise(): void
    {
        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'author_id' => $this->getUserID(),
                'language_id' => $language->id,
            ])
            ->with('tags', TagFactory::class)
            ->first();

        $response = $this->request(
            method: 'DELETE',
            path: sprintf('/exercises/%s', $exercise->id),
            options: [
                'headers' => [
                    'Authorization' => 'Bearer AbCdEf123456',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $this->assertDatabaseCount(Exercise::class, 0);
    }
}
