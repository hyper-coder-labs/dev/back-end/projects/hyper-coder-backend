<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise;

use App\Controller\AbstractController;
use App\Controller\Exercise\ExerciseShowController;
use App\Model\Exercise;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseShowControllerFTest extends HyperfTestCase
{
    public function testShowExerciseOnSuccess()
    {
        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'language_id' => $language->id,
            ])
            ->first();

        $response = $this->request(
            method: 'GET',
            path: sprintf('/exercises/%s', $exercise->id),
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertEquals(ExerciseShowController::MESSAGE_SUCCESS, $responseContents['message']);
        $this->assertEquals($exercise->id, $responseContents['data']['id']);
    }

    public function testShowExerciseNotFoundId()
    {
        $response = $this->request(
            method: 'GET',
            path: sprintf('/exercises/%s', 99),
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_NOT_FOUND, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertEquals(AbstractController::MESSAGE_NOT_FOUND, $responseContents['message']);
    }
}
