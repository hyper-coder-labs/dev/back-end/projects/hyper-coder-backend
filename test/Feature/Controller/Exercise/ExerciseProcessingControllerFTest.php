<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise;

use App\Model\Exercise;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\Helper\UseLocalData;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class ExerciseProcessingControllerFTest extends HyperfTestCase
{
    use UseLocalData;

    public function testProcessingExerciseOnSuccess(): void
    {
        $data = $this->getLocalDataJson('exercises', 'exercise-processing-request.json');

        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'author_id' => '1',
                'language_id' => $language->id,
            ])
            ->first();

        $response = $this->request(
            method: 'POST',
            path: sprintf('/exercises/%s/processing', $exercise->id),
            options: [
                'headers' => [
                    'Authorization' => 'Bearer AbCdEf123456',
                ],
                'form_params' => $data,
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
    }
}
