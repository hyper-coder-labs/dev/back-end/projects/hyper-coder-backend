<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Exercise\Practice;

use App\Controller\Exercise\Practice\PracticeIndexController;
use App\Model\Exercise;
use App\Model\Language;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class PracticeIndexControllerFTest extends HyperfTestCase
{
    public function testPracticeIndexOnSuccess()
    {
        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'language_id' => $language->id,
            ])
            ->first();

        $response = $this->request(
            method: 'GET',
            path: sprintf('/exercises/%s/practices', $exercise->id),
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertEquals(PracticeIndexController::MESSAGE_SUCCESS, $responseContents['message']);
    }
}
