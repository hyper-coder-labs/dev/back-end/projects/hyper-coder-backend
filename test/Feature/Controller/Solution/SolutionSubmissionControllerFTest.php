<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Solution;

use App\Controller\Solution\SolutionSubmissionController;
use App\Model\Exercise;
use App\Model\Language;
use Hyperf\Amqp\Producer;
use HyperfTest\Factory\ExerciseFactory;
use HyperfTest\Factory\LanguageFactory;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class SolutionSubmissionControllerFTest extends HyperfTestCase
{
    public function testSolutionSubmissionOnSuccess()
    {
        /** @var Language $language */
        $language = $this->factory(LanguageFactory::class)
            ->createOne()
            ->first();

        /** @var Exercise $exercise */
        $exercise = $this->factory(ExerciseFactory::class)
            ->createOne([
                'language_id' => $language->id,
            ])
            ->first();

        $practiceOid = '1f971d314da245f3a9dff8d77308bf94';
        $data = [
            'exercise_id' => $exercise->id,
            'files' => [
                [
                    'content' => 'Code',
                    'filename' => 'solution.php',
                ],
            ],
        ];

        $mockProducer = $this->mock(Producer::class);
        $mockProducer->allows('produce')
            ->withAnyArgs()
            ->andReturnTrue();

        $response = $this->request(
            method: 'POST',
            path: sprintf('/solutions/%s/submissions', $practiceOid),
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json' => $data,
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals(SolutionSubmissionController::MESSAGE_SUCCESS, $responseContents['message']);
        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('submission', $responseContents['data']);
        $this->assertArrayHasKey('id', $responseContents['data']['submission']);
        $this->assertArrayHasKey('queued', $responseContents['data']['submission']);

        $this->assertTrue($responseContents['data']['submission']['queued']);
    }
}
