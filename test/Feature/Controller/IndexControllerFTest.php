<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller;

use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class IndexControllerFTest extends HyperfTestCase
{
    public function testIndexController()
    {
        $response = $this->request(
            method: 'GET',
            path: '/',
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('exercises_url', $responseContents);
        $this->assertArrayHasKey('exercises_search_url', $responseContents);
    }
}
