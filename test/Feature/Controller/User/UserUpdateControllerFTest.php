<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\User;

use HyperfTest\Helper\KeycloakMock;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class UserUpdateControllerFTest extends HyperfTestCase
{
    use KeycloakMock;

    public function testUserUpdateOnSuccess()
    {
        $dataUpdate = [
            'bio' => 'Olá meu nome é Luiz',
            'gender' => 'male',
            'language' => 'pt_BR',
            'phone_number' => '68992454267',
            'date_birth' => '2001-03-08',
            'links' => [
                [
                    'text' => 'website',
                    'link' => 'https://gitlab.com/hyper-coder-labs',
                ],
            ],
            'address' => [
                'cep' => '69917748',
            ],
        ];

        $response = $this->request(
            method: 'PUT',
            path: '/user/info/',
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'form_params' => $dataUpdate,
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        sleep(2);
    }
}
