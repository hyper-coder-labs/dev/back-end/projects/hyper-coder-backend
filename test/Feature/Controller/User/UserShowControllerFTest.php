<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\User;

use App\Model\User\UserData;
use HyperfTest\Factory\Address\CountryFactory;
use HyperfTest\Factory\Address\MunicipalityFactory;
use HyperfTest\Factory\Address\StateFactory;
use HyperfTest\Factory\User\UserAdressFactory;
use HyperfTest\Factory\User\UserDataFactory;
use HyperfTest\Factory\User\UserLinkFactory;
use HyperfTest\Helper\KeycloakMock;
use HyperfTest\HyperfTestCase;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class UserShowControllerFTest extends HyperfTestCase
{
    use KeycloakMock;

    public function testShowUserOnSuccess()
    {
        $userID = $this->getUserID();

        /** @var UserData $userData */
        $userData = $this->factory(UserDataFactory::class)
            ->createOne(['user_id' => $userID])
            ->first();

        $this->factory(UserLinkFactory::class)
            ->createMany(4, ['user_id' => $userID])
            ->get();

        $country = $this->factory(CountryFactory::class)
            ->createOne()
            ->first();

        $state = $this->factory(StateFactory::class)
            ->createOne(['country_id' => $country->id])
            ->first();

        $municipality = $this->factory(MunicipalityFactory::class)
            ->createOne(['state_id' => $state->id])
            ->first();

        $this->factory(UserAdressFactory::class)
            ->createOne([
                'user_id' => $userID,
                'municipality_id' => $municipality->id,
            ])
            ->first();

        $response = $this->request(
            method: 'GET',
            path: '/user/info/',
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('address', $responseContents['data']);
        $this->assertArrayHasKey('links', $responseContents['data']);
        $this->assertArrayHasKey('municipality', $responseContents['data']['address']);

        $this->assertEquals($userData['bio'], $responseContents['data']['bio']);
        $this->assertEquals($userData['language'], $responseContents['data']['language']);
        $this->assertEquals($userData['phone_number'], $responseContents['data']['phone_number']);
        $this->assertEquals($userData['date_birth'], $responseContents['data']['date_birth']);
    }

    public function testShowUserEmptyDataOnSuccess()
    {
        $response = $this->request(
            method: 'GET',
            path: '/user/info/',
            options: [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());
        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);

        $this->assertNull($responseContents['data']['bio']);
        $this->assertNull($responseContents['data']['phone_number']);
        $this->assertNull($responseContents['data']['date_birth']);
        $this->assertNotNull($responseContents['data']['language']);
    }
}
