<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace Controller\Language;

use HyperfTest\Factory\LanguageFactory;
use HyperfTest\HyperfTestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as StatusCodes;

/**
 * @internal
 */
class LanguageIndexControllerFTest extends HyperfTestCase
{
    public function testIndexLanguage()
    {
        $factoryTotal = 10;

        $this->factory(LanguageFactory::class)
            ->createMany($factoryTotal)
            ->first();

        /** @var ResponseInterface $response */
        $response = $this->request(method: 'GET', path: '/languages', options: [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $this->assertEquals(StatusCodes::HTTP_OK, $response->getStatusCode());

        $responseContents = json_decode($response->getBody()->getContents(), true);

        $this->assertArrayHasKey('data', $responseContents);
        $this->assertArrayHasKey('total', $responseContents);
        $this->assertEquals($factoryTotal, $responseContents['total']);
    }
}
