<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Helper;

use Hyperf\Collection\Arr;
use Hyperf\Database\ConnectionInterface;
use Hyperf\DbConnection\Db;
use Hyperf\DbConnection\Model\Model;
use HyperfTest\Constraints\CountInDatabase;
use HyperfTest\Constraints\HasInDatabase;

trait InteractsWithDatabase
{
    /**
     * Seed a given database connection.
     *
     * @return $this
     */
    public function seed(string $class): static
    {
        foreach (Arr::wrap($class) as $class) {
            $this->runCommand('db:seed', 'database');
            // ['--class' => $class, '--no-interaction' => true]
        }

        return $this;
    }

    /**
     * Assert that a given where condition exists in the database.
     *
     * @param Model|string $table
     * @param null|string $connection
     * @return $this
     */
    protected function assertDatabaseHas($table, array $data, $connection = null): static
    {
        $this->assertThat(
            $this->getTable($table),
            new HasInDatabase($this->getConnection($connection, $table), $data)
        );

        return $this;
    }

    /**
     * Assert the count of table entries.
     *
     * @param Model|string $table
     * @param null|string $connection
     * @return $this
     */
    protected function assertDatabaseCount($table, int $count, $connection = null): static
    {
        $this->assertThat(
            $this->getTable($table),
            new CountInDatabase($this->getConnection($connection, $table), $count)
        );

        return $this;
    }

    /**
     * Assert that the given table has no entries.
     *
     * @param Model|string $table
     * @param null|string $connection
     * @return $this
     */
    protected function assertDatabaseEmpty($table, $connection = null): static
    {
        $this->assertThat(
            $this->getTable($table),
            new CountInDatabase($this->getConnection($connection, $table), 0)
        );

        return $this;
    }

    /**
     * Get the database connection.
     */
    protected function getConnection(?string $connection = null, ?string $table = null): ConnectionInterface
    {
        if ($connection) {
            return Db::connection($connection);
        }

        return $this->getContainer()->get(Model::class)->getConnection();
    }

    /**
     * Get the table name from the given model or string.
     *
     * @param Model|string $table
     * @return string
     */
    protected function getTable($table)
    {
        return $this->newModelFor($table)?->getTable() ?: $table;
    }

    /**
     * Get the table connection specified in the given model.
     *
     * @param Model|string $table
     */
    protected function getTableConnection($table): ?string
    {
        return $this->newModelFor($table)?->getConnectionName();
    }

    /**
     * Get the model entity from the given model or string.
     *
     * @param Model|string $table
     */
    protected function newModelFor($table): ?Model
    {
        return is_subclass_of($table, Model::class) ? (new $table()) : null;
    }
}
