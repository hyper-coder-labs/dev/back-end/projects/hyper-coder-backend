<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Helper;

use Mockery\MockInterface;
use Stevenmaguire\OAuth2\Client\Provider\Keycloak;
use Stevenmaguire\OAuth2\Client\Provider\KeycloakResourceOwner;

trait KeycloakMock
{
    public function getUserID(): string
    {
        return '3074d846-94a3-4cef-9181-9bd8daa4b926';
    }

    public function createKeycloakMock(string $userId = '1'): MockInterface
    {
        $resourceOwner = new KeycloakResourceOwner([
            'sub' => $userId,
            'email' => 'user@test.com',
            'name' => 'User Test',
        ]);

        $mock = $this->mock(Keycloak::class);
        $mock->allows('getResourceOwner')
            ->withAnyArgs()
            ->andReturns($resourceOwner);

        return $mock;
    }
}
