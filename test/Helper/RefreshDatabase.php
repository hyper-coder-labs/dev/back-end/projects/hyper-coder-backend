<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Helper;

use function Hyperf\Config\config;

trait RefreshDatabase
{
    /**
     * Indicates if the test database has been migrated.
     */
    protected static bool $migrated = false;

    /**
     * Define hooks to migrate the database before and after each test.
     */
    public function refreshDatabase(): void
    {
        $this->usingInMemoryDatabase()
            ? $this->refreshInMemoryDatabase()
            : $this->refreshTestDatabase();
    }

    public function closeDatabase(): void
    {
        if (! $this->usingInMemoryDatabase()) {
            $this->runCommand('migrate:fresh', 'database');
            // $this->rollBackDatabaseTransaction();
        }
    }

    /**
     * Begin a database transaction on the testing database.
     */
    public function beginDatabaseTransaction(): void
    {
        // Db::connection('default')->beginTransaction();
    }

    public function rollBackDatabaseTransaction(): void
    {
        // Db::connection('default')->rollBack();
    }

    /**
     * Determine if an in-memory database is being used.
     */
    protected function usingInMemoryDatabase(): bool
    {
        return config('databases')['default']['database'] == ':memory:';
    }

    /**
     * Refresh the in-memory database.
     */
    protected function refreshInMemoryDatabase(): void
    {
        $this->runCommand('migrate', 'database');
    }

    /**
     * Refresh a conventional test database.
     */
    protected function refreshTestDatabase(): void
    {
        if (! static::$migrated) {
            $this->runCommand('migrate:fresh', 'database');
            static::$migrated = true;
        }

        // $this->beginDatabaseTransaction();
    }
}
