<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Helper;

use Hyperf\Contract\ApplicationInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;

trait HyperfConsoleTest
{
    public function runCommand(string $command, string $output = 'cli'): int
    {
        $application = $this->getContainer()->get(ApplicationInterface::class);

        $fileOutput = BASE_PATH . "/runtime/logs/{$output}.log";
        if (! file_exists($fileOutput)) {
            mkdir(dirname($fileOutput), 0755, true);
        }

        $input = new ArrayInput(['command' => $command]);
        $output = new StreamOutput(fopen($fileOutput, 'w'));

        $command = $application->get($command);
        return $command->run($input, $output);
    }
}
