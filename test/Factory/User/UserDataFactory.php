<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory\User;

use App\Model\User\UserData;
use Faker\Generator;
use HyperfTest\Factory\Factory;
use HyperfTest\Factory\FakerFactory;

class UserDataFactory extends Factory
{
    protected string $model = UserData::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'bio' => $this->faker->paragraph,
            'gender' => 'male',
            'language' => $this->faker->randomElement(['pt_BR', 'pt', 'en', 'es']),
            'phone_number' => $this->faker->text,
            'date_birth' => $this->faker->dateTime,
        ];
    }
}
