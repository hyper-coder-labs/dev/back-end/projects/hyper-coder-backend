<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory\User;

use App\Model\User\UserLink;
use Faker\Generator;
use HyperfTest\Factory\Factory;
use HyperfTest\Factory\FakerFactory;

class UserLinkFactory extends Factory
{
    protected string $model = UserLink::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'text' => $this->faker->randomElement(['website', 'github']),
            'link' => $this->faker->url,
        ];
    }
}
