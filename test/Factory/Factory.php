<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use App\Model\Model;
use Faker\Generator;

abstract class Factory
{
    public readonly Generator $faker;

    protected string $model;

    private int $count;

    private array $data = [];

    public function __construct()
    {
        $this->faker = $this->withFaker();
    }

    abstract public function withFaker(): Generator;

    abstract public function definition(): array;

    public function createOne(array $attributes = []): static
    {
        return $this->count(1)->create($attributes);
    }

    public function createMany(int $count = 1, array $attributes = []): static
    {
        return $this->count($count)->create($attributes);
    }

    public function with(string $relation, string $factory, array $attributes = []): static
    {
        $factory = \Hyperf\Support\make($factory);

        for ($i = 0; $i < sizeof($this->data); ++$i) {
            $attributes = array_merge($factory->definition(), $attributes);
            $this->data[$i]->{$relation}()->create($attributes);
        }

        return $this;
    }

    public function get(): array
    {
        return $this->data;
    }

    public function map(callable $callable) : static
    {
        foreach ($this->data as $item) {
            $callable($item);
        }

        return $this;
    }

    /**
     * @return Model
     */
    public function first()
    {
        return $this->data[0];
    }

    private function count(int $value): static
    {
        $this->count = $value;
        return $this;
    }

    private function create($attributes = []): static
    {
        $this->data = [];
        for ($i = 0; $i < $this->count; ++$i) {
            $attributes = array_merge($this->definition(), $attributes);
            $this->data[] = $this->model::create($attributes);
        }

        $this->count(0);
        return $this;
    }
}
