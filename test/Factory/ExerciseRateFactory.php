<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use App\Model\ExerciseRate;
use Faker\Generator;

class ExerciseRateFactory extends Factory
{
    protected string $model = ExerciseRate::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'exercise_id' => $this->faker->randomDigit(),
            'user_id' => $this->faker->randomDigit(),
            'rate' => $this->faker->randomElement([1, 2, 3, 4, 5]),
            'comment' => $this->faker->paragraph,
        ];
    }
}
