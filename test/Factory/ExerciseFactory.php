<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use App\Model\Exercise;
use Faker\Generator;

class ExerciseFactory extends Factory
{
    protected string $model = Exercise::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'author_id' => $this->faker->randomDigit(),
            'slug' => $this->faker->slug,
            'title' => $this->faker->title,
            'subtitle' => $this->faker->title,
            'description' => $this->faker->paragraph,
        ];
    }
}
