<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use App\Model\Language;
use Faker\Generator;
use Hyperf\Stringable\Str;

class LanguageFactory extends Factory
{
    protected string $model = Language::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'name' => $this->faker->languageCode,
            'slug' => Str::slug($this->faker->languageCode),
            'icon_url' => $this->faker->url,
        ];
    }
}
