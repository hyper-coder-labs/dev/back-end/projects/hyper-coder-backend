<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory\Address;

use App\Model\Address\State;
use Faker\Generator;
use HyperfTest\Factory\Factory;
use HyperfTest\Factory\FakerFactory;

class StateFactory extends Factory
{
    protected string $model = State::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'code' => $this->faker->stateAbbr,
            'name' => $this->faker->state,
        ];
    }
}
