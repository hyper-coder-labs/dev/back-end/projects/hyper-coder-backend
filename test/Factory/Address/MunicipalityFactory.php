<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory\Address;

use App\Model\Address\Municipality;
use Faker\Generator;
use HyperfTest\Factory\Factory;
use HyperfTest\Factory\FakerFactory;

class MunicipalityFactory extends Factory
{
    protected string $model = Municipality::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'name' => $this->faker->city,
        ];
    }
}
