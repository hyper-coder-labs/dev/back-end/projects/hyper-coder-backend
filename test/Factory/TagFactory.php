<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use App\Model\Tag;
use Faker\Generator;

class TagFactory extends Factory
{
    protected string $model = Tag::class;

    public function withFaker(): Generator
    {
        return FakerFactory::create();
    }

    public function definition(): array
    {
        return [
            'tag' => $this->faker->randomElement(['JAVA', 'PHP', 'C#', 'GO']),
        ];
    }
}
