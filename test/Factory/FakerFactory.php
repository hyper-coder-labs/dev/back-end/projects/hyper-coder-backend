<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest\Factory;

use Faker\Factory;
use Faker\Generator;
use Faker\Provider\pt_BR\Address;
use Faker\Provider\pt_BR\Payment;
use Faker\Provider\pt_BR\Person;
use Faker\Provider\pt_BR\PhoneNumber;

class FakerFactory
{
    public static function create(): Generator
    {
        $faker = Factory::create('pt_BR');

        $faker->addProvider(new Address($faker));
        $faker->addProvider(new Person($faker));
        $faker->addProvider(new Payment($faker));
        $faker->addProvider(new PhoneNumber($faker));

        return $faker;
    }
}
