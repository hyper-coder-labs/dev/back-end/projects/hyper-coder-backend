<?php

declare(strict_types=1);
/**
 * This file is part of HyperCoder.
 *
 * @link     https://gitlab.com/hyper-coder-labs
 * @author   Reinan Gabriel
 * @contact  @HyperCoder
 */
namespace HyperfTest;

use Hyperf\Context\ApplicationContext;
use Hyperf\Testing\Client;
use Hyperf\Testing\Concerns\InteractsWithContainer;
use HyperfTest\Factory\Factory;
use HyperfTest\Helper\HyperfConsoleTest;
use HyperfTest\Helper\InteractsWithDatabase;
use HyperfTest\Helper\RefreshDatabase;
use Mockery as m;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Throwable;

use function Hyperf\Support\make;

/**
 * Class HttpTestCase.
 * @method get($uri, $data = [], $headers = [])
 * @method post($uri, $data = [], $headers = [])
 * @method json($uri, $data = [], $headers = [])
 * @method file($uri, $data = [], $headers = [])
 * @method request($method, $path, $options = [])
 */
abstract class HyperfTestCase extends TestCase
{
    use HyperfConsoleTest;
    use RefreshDatabase;
    use InteractsWithDatabase;
    use InteractsWithContainer;

    protected Client $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->client = make(Client::class);
    }

    public function __call($name, $arguments)
    {
        return $this->client->{$name}(...$arguments);
    }

    protected function setUp(): void
    {
        parent::setUp();

        if (! $this->container) {
            // Desabilitado temporariamente porque está apresentando falha na criação do novo container.
            // $this->refreshContainer();

            $this->container = ApplicationContext::getContainer();
        }

        $this->refreshDatabase();
    }

    protected function tearDown(): void
    {
        $this->closeDatabase();
        $this->container = null;

        try {
            m::close();
        } catch (Throwable) {
        }

        parent::tearDown();
    }

    public function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    public function setMock(string $class, MockObject $mockObject): void
    {
        $this->getContainer()->set($class, $mockObject);
    }

    public function factory(string $factory): Factory
    {
        return make($factory);
    }
}
